import AppKit

class ColorsViewController: NSViewController {
    lazy var colors: [NSColor] = systemColors()
    
    private func systemColors() -> [NSColor] {
        guard let list = NSColorList.availableColorLists.first(where: { $0.name == "System" }) else { return [] }
        return list.allKeys.compactMap{ list.color(withKey: $0) }
    }
}

extension ColorsViewController: NSTableViewDataSource {
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        return colors.count
    }
}

extension ColorsViewController: NSTableViewDelegate {
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let view = tableView.makeView(withIdentifier: .init("Cell"), owner: self)
        guard let cell = view as? ColorCellView else { return view }
        
        cell.color = colors[row]        
        return cell
    }
}
