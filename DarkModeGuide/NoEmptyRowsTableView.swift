import Cocoa

class NoEmptyRowsTableView: NSTableView {
    override func drawGrid(inClipRect clipRect: NSRect) {}
}
