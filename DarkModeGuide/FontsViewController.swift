import AppKit

class FontsViewController: NSViewController {
    lazy var fontNames = Array(systemFontVariants.keys).sorted()
}

extension FontsViewController: NSTableViewDataSource {
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        return fontNames.count
    }
}

extension FontsViewController: NSTableViewDelegate {
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let view = tableView.makeView(withIdentifier: .init("Cell"), owner: self)
        guard let cell = view as? FontCellView else { return view }
        
        let fontName = fontNames[row]
        guard let font = systemFontVariants[fontName] else { return view }
        
        cell.titleTextField.stringValue = fontName
        cell.exampleTextField.font = font
        cell.descriptionTextField.stringValue = "\(font.displayName ?? "") \(font.pointSize)"
        return cell
    }
}

private let systemFontVariants = [
    "Control Content": NSFont.controlContentFont(ofSize: 0),
    "Label": NSFont.labelFont(ofSize: 0),
    "Menu": NSFont.menuFont(ofSize: 0),
    "Menu Bar": NSFont.menuBarFont(ofSize: 0),
    "Message": NSFont.messageFont(ofSize: 0),
    "Palette": NSFont.paletteFont(ofSize: 0),
    "Title": NSFont.titleBarFont(ofSize: 0),
    "Tool Tips": NSFont.toolTipsFont(ofSize: 0),
    "Bold System Font": NSFont.boldSystemFont(ofSize: 0),
    "System Font": NSFont.systemFont(ofSize: 0)
]
