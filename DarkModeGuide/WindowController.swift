import AppKit

class WindowController: NSWindowController {
    
    @IBAction func appearanceSegmentedControl(_ sender: NSSegmentedControl) {
        switch sender.selectedSegment {
        case 0: window?.appearance = NSApp.appearance
        case 1: window?.appearance = NSAppearance(named: .aqua)
        case 2: window?.appearance = NSAppearance(named: .darkAqua)
        default: break
        }
    }
}
