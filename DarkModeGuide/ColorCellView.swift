import AppKit

class ColorCellView: NSTableCellView {
    
    @IBOutlet private weak var titleTextField: NSTextField!
    @IBOutlet private weak var colorTextField: NSTextField!
    @IBOutlet private weak var descriptionTextField: NSTextField!
    @IBOutlet private weak var colorBox: NSBox!
    
    var color: NSColor = .black {
        didSet { updateUI() }
    }
    
    override func viewDidChangeEffectiveAppearance() {
        colorTextField.stringValue = color.rgbDescription
    }
}

private extension ColorCellView {
    
    func updateUI() {
        colorBox.fillColor = color
        titleTextField.stringValue = color.colorNameComponent
        colorTextField.stringValue = color.rgbDescription
        descriptionTextField.stringValue = colorDescriptions[color.colorNameComponent] ?? "No description."
    }
}

private extension NSColor {
    
    var rgbDescription: String {
        guard let rgbColor = usingColorSpace(.extendedSRGB) else { return "Can't get RGB values" }
        
        let red = Int(rgbColor.redComponent * 255)
        let green = Int(rgbColor.greenComponent * 255)
        let blue = Int(rgbColor.blueComponent * 255)
        let alpha = Int(rgbColor.alphaComponent * 100)
        return String(format: "RGB: (%1$d, %2$d, %3$d), Hex: #%1$02X%2$02X%3$02X, Alpha: %4$d%%", red, green, blue, alpha)
    }
}

private let colorDescriptions = [
    "labelColor": "The primary color to use for text labels.",
    "secondaryLabelColor": "The secondary color to use for text labels.",
    "tertiaryLabelColor": "The tertiary color to use for text labels.",
    "quaternaryLabelColor": "The quaternary color to use for text labels and separators.",
    "textColor": "The color to use for text.",
    "placeholderTextColor": "The color to use for placeholder text in controls or text views.",
    "selectedTextColor": "The color to use for selected text.",
    "textBackgroundColor": "The color to use for the background area behind text.",
    "selectedTextBackgroundColor": "The color to use for the background of selected text.",
    "keyboardFocusIndicatorColor": "The color to use for the keyboard focus ring around controls.",
    "unemphasizedSelectedTextColor": "The color to use for selected text in an unemphasized context.",
    "unemphasizedSelectedTextBackgroundColor": "The color to use for the text background in an unemphasized context.",
    "linkColor": "The color to use for links.",
    "separatorColor": "The color to use for separators between different sections of content.",
    "selectedContentBackgroundColor": "The color to use for the background of selected and emphasized content.",
    "unemphasizedSelectedContentBackgroundColor": "The color to use for selected and unemphasized content.",
    "selectedMenuItemTextColor": "The color to use for the text in menu items.",
    "gridColor": "The color to use for the optional gridlines, such as those in a table view.",
    "headerTextColor": "The color to use for text in header cells in table views and outline views.",
    "alternatingContentBackgroundColors": "The colors to use for alternating content, typically found in table views and collection views.",
    "controlAccentColor": "The user's current accent color preference.",
    "controlColor": "The color to use for the flat surfaces of a control.",
    "controlBackgroundColor": "The color to use for the background of large controls, such as scroll views or table views.",
    "controlTextColor": "The color to use for text on enabled controls.",
    "disabledControlTextColor": "The color to use for text on disabled controls.",
    "currentControlTint": "The current system control tint color.",
    "selectedControlColor": "The color to use for the face of a selected control—that is, a control that has been clicked or is being dragged.",
    "selectedControlTextColor": "The color to use for text in a selected control—that is, a control being clicked or dragged.",
    "alternateSelectedControlTextColor": "The color to use for text in a selected control.",
    "scrubberTexturedBackground": "The patterned color to use for the background of a scrubber control. Window Colors",
    "windowBackgroundColor": "The color to use for the window background.",
    "windowFrameTextColor": "The color to use for text in a window's frame.",
    "underPageBackgroundColor": "The color to use in the area beneath your window's views. Highlights and Shadows",
    "findHighlightColor": "The highlight color to use for the bubble that shows inline search result values.",
    "highlightColor": "The color to use as a virtual light source on the screen.",
    "shadowColor": "The color to use for virtual shadows cast by raised objects on the screen. (Deprecated)",
    "alternateSelectedControlColor": "The system color used for the face of a selected control in a list or table. (Deprecated)",
    "controlAlternatingRowBackgroundColors": "An array containing the system specified background colors for alternating rows in tables and lists. (Deprecated)",
    "controlHighlightColor": "The system color used for the highlighted bezels of controls. (Deprecated)",
    "controlLightHighlightColor": "The system color used for light highlights in controls. (Deprecated)",
    "controlShadowColor": "The system color used for the shadows dropped from controls. (Deprecated)",
    "controlDarkShadowColor": "The system color used for the dark edge of the shadow dropped from controls. (Deprecated)",
    "headerColor": "The system color used as the background color for header cells in table views and outline views. (Deprecated)",
    "knobColor": "The system color used for the flat surface of a slider knob that hasn’t been selected. (Deprecated)",
    "selectedKnobColor": "The system color used for the slider knob when it is selected. (Deprecated)",
    "scrollBarColor": "The system color used for scroll “bars”—that is, for the groove in which a scroller’s knob moves (Deprecated)",
    "secondarySelectedControlColor": "The color used for selected controls in non-key views. (Deprecated)",
    "selectedMenuItemColor": "The color to use for the face of selected menu items. (Deprecated)",
    "windowFrameColor": "The system color used for window frames, except for their text. (Deprecated)"
]
